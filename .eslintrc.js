module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": "plugin:react/recommended",
    "overrides": [
    ],
    "parserOptions": {
        "parser": '@babel/eslint-parser',
        "requireConfigFile": false,
        "ecmaVersion": "latest",
        "sourceType": "module",
        "babelOptions": {
            "parserOpts": {
              "plugins": ["jsx"]
            }
          }
    },
    "parser": "@babel/eslint-parser",
    "plugins": [
        "react"
    ],
    "rules": {
       "react/react-in-jsx-scope": "off"
    },
    "settings": {
        "react": {
          "version": "detect"
        }
      }
}
